﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterCountMismatch
{
    internal class ClassUnderTest
    {
        private ClassToMock _instance;

        internal ClassUnderTest(ClassToMock instanceToUse)
        {
            _instance = instanceToUse;
        }

        internal int MethodUsingInstance()
        {
            var x = _instance.MethodWithParameterChange("this is a string", 5, true);
            return x * 2;
        }
    }
}
