﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterCountMismatch
{
    internal class ClassToMock
    {
        internal virtual int MethodWithParameterChange(string someString, int someInt, bool someBool)
        {
            return 42;
        }
    }
}
