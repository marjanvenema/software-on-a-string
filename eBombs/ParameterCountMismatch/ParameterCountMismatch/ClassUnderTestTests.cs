﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ParameterCountMismatch
{
    [TestClass]
    public class ClassUnderTestTests
    {
        [TestMethod]
        public void MethodUsingInstance_ShouldReturn_DoubleTheValueOfItsInstance()
        {
            // Arrange
            var instanceToUse = Mock.Of<ClassToMock>();
            Mock.Get(instanceToUse)
                .Setup(instance => instance.MethodWithParameterChange(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns((string s, int i, bool b) => 23);

            var cut = new ClassUnderTest(instanceToUse);

            // Act
            var returned = cut.MethodUsingInstance();

            // Assert
            Assert.AreEqual(46, returned);
        }
    }
}
