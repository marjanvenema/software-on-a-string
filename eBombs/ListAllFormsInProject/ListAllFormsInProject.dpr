program ListAllFormsInProject;

uses
  Vcl.Forms,
  Main_fm in 'Main_fm.pas' {MainForm},
  Main_data in 'Main_data.pas' {MainData: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TMainData, MainData);
  Application.Run;
end.
