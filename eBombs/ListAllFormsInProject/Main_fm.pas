unit Main_fm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TMainForm = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure DigDownComponent(const aComponent: TComponent; const aList: TStrings; const aPrefix: string); forward;

procedure DigDownApplicationComponents;
var
  FormList: TStringList;
begin
  FormList := TStringList.Create;
  try
    DigDownComponent(Application, FormList, '');
  finally
    FormList.Free;
  end;
end;

procedure DigDownComponent(const aComponent: TComponent; const aList: TStrings; const aPrefix: string);
var
  idx: Integer;
  Current: TComponent;
begin
  for idx := 0 to aComponent.ComponentCount - 1 do
  begin
    Current := aComponent.Components[idx];

    if Current.InheritsFrom(TCustomForm) then
      aList.Add(Format('%s%s (%s)', [aPrefix, Current.Name, Current.ClassName]));

    if Current.ComponentCount > 0 then
      DigDownComponent(Current, aList, aPrefix + '  ');
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  Component: TComponent;
  idx: Integer;
  CustomForm: TCustomForm;
begin
  for idx := 0 to Screen.CustomFormCount - 1 do
  begin
    CustomForm := Screen.CustomForms[idx];
    Memo1.Lines.Add(Format('%s (%s)', [CustomForm.Name, CustomForm.ClassName]));
  end;

  for idx := 0 to Application.ComponentCount - 1 do
  begin
    Component := Application.Components[idx];
    if Component.InheritsFrom(TCustomForm) then
      // Whatever you want to do with them. Me, I am just adding them to a Memo.
      Memo1.Lines.Add(Format('%s (%s)', [Component.Name, Component.ClassName]));
  end;
end;

end.
