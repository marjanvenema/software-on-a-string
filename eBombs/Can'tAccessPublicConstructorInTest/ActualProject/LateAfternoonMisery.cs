﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject
{
    /*public*/ class LateAfternoonMisery
    {
        private int _MyInteger;

        public int AwesomeProperty { get {return _MyInteger; } }

        public LateAfternoonMisery()
        {
            _MyInteger = 42;
        }
    }
}
