﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject;

namespace TestProject
{
    [TestClass]
    public class LateAfternoonMisery_Tests
    {
        [TestMethod]
        public void LateAfternoonMisery_WhenConstructed_AwesomeProperty_ShouldReturn_DefaultValue()
        {
            var myLateAfternoonMisery = new LateAfternoonMisery();
            Assert.AreEqual(myLateAfternoonMisery.AwesomeProperty, 42);
        }
    }
}
