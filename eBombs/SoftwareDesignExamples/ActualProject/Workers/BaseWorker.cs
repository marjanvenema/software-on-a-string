﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.Workers
{
    class WorkerError : Exception 
    { 
        public WorkerError(string message) : base(message) {}
    }
    class CanHandleNotCheckedError : WorkerError 
    { 
        public CanHandleNotCheckedError(string message) : base(message) {}
    }

    class BaseWorker : IWorker
    {
        public Boolean CanHandle(IWork work) { return false; }
        public void Handle(IWork work) 
        {
            Debug.Assert(CanHandle(work), "You should call CanHandle before calling Handle");

            Trace.Assert(CanHandle(work), "You should call CanHandle before calling Handle");

            if (!CanHandle(work))
                throw new CanHandleNotCheckedError("You should call CanHandle before calling Handle");
        }
        public Boolean Handle(IWork work)
        {
            return false;
        }
    }
}
