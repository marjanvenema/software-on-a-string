﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.Workers
{
    interface IWork
    {
        // ... snip ...
    }

    enum AcceptWorkResponse
    {
        Rejected,
        Accepted
    }

    interface IWorker
    {
        public Boolean CanHandle(IWork work);
        public void Handle(IWork work);
        public AcceptWorkResponse Accept(IWork work);
    }
}
