﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.Workers
{
    class WorkDistributor
    {
        ICollection<IWork> _Workload;
        ICollection<IWorker> _Workers;

        public void Distribute(IWork work)
        {
            foreach (var worker in _Workers)
            {
                if (worker.CanHandle(work))
                {
                    worker.Handle(work);
                    break;
                }
            }
        }

        public void Distribute2(IWork work)
        {
            foreach (var worker in _Workers)
            {
                if (worker.Accept(work) == AcceptWorkResponse.Accepted)
                    break;
            }
        }
    }
}
