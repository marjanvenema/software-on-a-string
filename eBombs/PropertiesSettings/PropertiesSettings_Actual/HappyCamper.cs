﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertiesSettings_Actual
{
    class HappyCamper
    {
        Properties.Settings _Settings;

        public HappyCamper(Properties.Settings useThisSettingsInstance)
        {
            _Settings = useThisSettingsInstance;
        }

        public string MethodReadingSomeSetting()
        {
            string LastFolder = _Settings.LastUsedFolder;
            return LastFolder;
        }

        public void MethodWritingSomeSetting(string newValue)
        {
            _Settings.LastUsedFolder = newValue;
            _Settings.Save();
        }
    }

    class EvenHappierCamper
    {
        HappySettings _Settings;

        public EvenHappierCamper(HappySettings useThisSettingsInstance)
        {
            _Settings = useThisSettingsInstance;
        }

        public string MethodReadingSomeSetting()
        {
            string LastFolder = _Settings.LastUsedFolder;
            return LastFolder;
        }

        public void MethodWritingSomeSetting(string newValue)
        {
            _Settings.LastUsedFolder = newValue;
            _Settings.Save();
        }
    }
}
