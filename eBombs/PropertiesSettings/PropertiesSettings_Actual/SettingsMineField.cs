﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertiesSettings_Actual
{
    class SettingsMineField
    {
        public string MethodReadingSomeSetting()
        {
            string LastFolder = Properties.Settings.Default.LastUsedFolder;
            return LastFolder;
        }

        public void MethodWritingSomeSetting(string newValue)
        {
            Properties.Settings.Default.LastUsedFolder = newValue;
            Properties.Settings.Default.Save();
        }
    }
}
