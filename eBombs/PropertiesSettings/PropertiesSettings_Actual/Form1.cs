﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PropertiesSettings_Actual
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var x = new SettingsMineField();
            textBox1.Text = x.MethodReadingSomeSetting();

            var y = new HappyCamper(Properties.Settings.Default);
            textBox1.Text = y.MethodReadingSomeSetting();

            // uses whatever default is coded into HappySettingsTwo
            var z = new EvenHappierCamper(new HappySettings()); 
            textBox1.Text = z.MethodReadingSomeSetting();
            // uses instance passed into HappySettingsTwo
            var z1 = new EvenHappierCamper(new HappySettings(Properties.Settings.Default));
            textBox1.Text = z1.MethodReadingSomeSetting();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var x = new SettingsMineField();
            x.MethodWritingSomeSetting(textBox1.Text);

            var y = new HappyCamper(Properties.Settings.Default);
            y.MethodWritingSomeSetting(textBox1.Text);
        }
    }
}
