﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertiesSettings_Actual
{
    class HappySettings
    {
        Properties.Settings _Settings;

        public HappySettings()
        {
            _Settings = Properties.Settings.Default;
        }

        public HappySettings(Properties.Settings useThisSettingsInstance)
        {
            _Settings = useThisSettingsInstance;
        }

        public string LastUsedFolder { get { return _Settings.LastUsedFolder; } set { _Settings.LastUsedFolder = value; } }
        public void Save() { _Settings.Save(); }
    }


}
