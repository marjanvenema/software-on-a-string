﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToArrayConsole
{
    interface IDoggies : IEnumerable<IDog>
    {
        void AddDog(string name, string breed, string gender, bool neutered);
    }
}
