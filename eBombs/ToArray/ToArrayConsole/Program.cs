﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToArrayConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating kennel");
            var kennel = new Kennel();

            Console.WriteLine("Getting ref1 to kennel's population");
            var ref1 = kennel.Population;

            Console.WriteLine("Enumerating kennel.population");
            foreach (var doggy in kennel.Population)
            {
                Console.WriteLine("kennel.population Doggie is {0}", doggy.Name);
            }

            Console.WriteLine("Enumerating ref1");
            foreach (var doggy in ref1)
            {
                Console.WriteLine("ref1 Doggie is {0}", doggy.Name);
            }

            Console.WriteLine("ToArray()");
            var toarray = kennel.Population.ToArray();

            Console.WriteLine("Enumerating toarray");
            foreach (var doggy in toarray)
            {
                Console.WriteLine("toarray Doggie is {0}", doggy.Name);
            }

            Console.WriteLine("Getting ref2 to kennel's population");
            var ref2 = kennel.Population;

            Console.WriteLine("Enumerating ref2");
            foreach (var doggy in ref2)
            {
                Console.WriteLine("ref2 Doggie is {0}", doggy.Name);
            }
        }
    }
}
