﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToArrayConsole
{
    class Doggies : IDoggies
    {
        private readonly Dictionary<string, IDog> _doggies = new Dictionary<string, IDog>();

        public IEnumerator<IDog> GetEnumerator()
        {
            return _doggies.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void AddDog(string name, string breed, string gender, bool neutered)
        {
            _doggies.Add(name, new Dog(name, breed, gender, neutered));
        }
    }
}
