﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToArrayConsole
{
    interface IDog
    {
        string Name { get; }

        string Breed { get; }

        string Gender { get; }

        bool Neutered { get; }
    }
}
