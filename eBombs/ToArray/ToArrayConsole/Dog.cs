﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToArrayConsole
{
    class Dog : IDog
    {
        public string Name { get; }
        public string Breed { get; }
        public string Gender { get; }
        public bool Neutered { get; }

        internal Dog(string name, string breed, string gender, bool neutered)
        {
            Name = name;
            Breed = breed;
            Gender = gender;
            Neutered = neutered;
        }
    }
}
