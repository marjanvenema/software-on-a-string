﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToArrayConsole
{
    class Kennel
    {
        internal IDoggies Population {
            get
            {
                Console.WriteLine("Enter Kennel.Population Get");
                try
                {
                    var population = new Doggies();
                    population.AddDog("Sofie", "Labrador", "female", true);
                    population.AddDog("Jessy", "Labrador", "female", true);
                    population.AddDog("BeeJay", "Labrador", "female", true);
                    return population;
                }
                finally
                {
                    Console.WriteLine("Exit- Kennel.Population Get");
                }
            }
        }
    }
}
