﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabet
{
    class Program
    {
        static void Main(string[] args)
        {
            for (char c = 'A'; c <= 'Z'; c++)
            {
                Console.Write(c);
                Console.ReadKey();
            }
        }
    }
}
