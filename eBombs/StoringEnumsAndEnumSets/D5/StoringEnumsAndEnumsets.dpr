program StoringEnumsAndEnumsets;

{$APPTYPE CONSOLE}

uses
  Classes,
  SysUtils,
  TypInfo;

const
  BOOLSTR: array [Boolean] of string = ('No', 'Yes');

type
  TAgeCategory = (acLongInTheTooth, acGreenLeaf, acOldHat, acFairMaiden);
  TAgeCategories = set of TAgeCategory;

  TEnum7 = (e7One, e7Two, e7Three, e7Four, e7Five, e7Six, e7Seven);
  TEnum7Set = set of TEnum7;

  TEnum8 = (e8One, e8Two, e8Three, e8Four, e8Five, e8Six, e8Seven, e8Eight);
  TEnum8Set = set of TEnum8;

  TEnum9 = (e9One, e9Two, e9Three, e9Four, e9Five, e9Six, e9Seven, e9Eight, e9Nine);
  TEnum9Set = set of TEnum9;

  TEnum15 = (e15One, e15Two, e15Three, e15Four, e15Five, e15Six, e15Seven, e15Eight,
    e15Nine, e15Ten, e15Eleven, e15Twelve, e15Thirtheen, e15Fourteen, e15Fifteen);
  TEnum15Set = set of TEnum15;

  TEnum16 = (e16One, e16Two, e16Three, e16Four, e16Five, e16Six, e16Seven, e16Eight,
    e16Nine, e16Ten, e16Eleven, e16Twelve, e16Thirtheen, e16Fourteen, e16Fifteen,
    e16Sixteen);
  TEnum16Set = set of TEnum16;

  TEnum17 = (e17One, e17Two, e17Three, e17Four, e17Five, e17Six, e17Seven, e17Eight,
    e17Nine, e17Ten, e17Eleven, e17Twelve, e17Thirtheen, e17Fourteen, e17Fifteen,
    e17Sixteen, e17Seventeen);
  TEnum17Set = set of TEnum17;

function EnumToString(aTypeInfo: PTypeInfo; aValue: Integer): string;
begin
  Assert(aTypeInfo.Kind = tkEnumeration, 'TypeInfo is not that of an enumeration');

  Result := GetEnumName(aTypeInfo, aValue);
end;

function StringToEnum(aTypeInfo: PTypeInfo; aValue: string): Integer;
begin
  Assert(aTypeInfo.Kind = tkEnumeration, 'TypeInfo is not that of an enumeration');

  Result := GetEnumValue(aTypeInfo, aValue);
end;

function EnumSetToString(aTypeInfo: PTypeInfo; aValue: Integer): string;
var
  EnumInfo: PTypeInfo;
  EnumTypeData: PTypeData;
  ValueAsIntegerSet: TIntegerSet;
  idx: Integer;
begin
  Assert(aTypeInfo.Kind = tkSet, 'TypeInfo is not that of an enumeration set');
  Result := '';

  EnumInfo := GetTypeData(aTypeInfo).CompType^;
  EnumTypeData := GetTypeData(EnumInfo);

  Integer(ValueAsIntegerSet) := aValue;
  for idx := EnumTypeData.MinValue to EnumTypeData.MaxValue do
    if idx in ValueAsIntegerSet then
      Result := Result + ', ' + GetEnumName(EnumInfo, idx);

  Result := Copy(Result, 3, Length(Result) - 2);
end;

function StringToEnumSet(aTypeInfo: PTypeInfo; aValue: string): Integer;
var
  EnumInfo: PTypeInfo;
  List: TStringList;
  idx: Integer;
  EnumValue: Integer;
  Name: string;
begin
  Assert(aTypeInfo.Kind = tkSet, 'TypeInfo is not that of an enumeration set');
  Result := 0;

  EnumInfo := GetTypeData(aTypeInfo).CompType^;

  List := TStringList.Create;
  try
    // D6_UP
    // List.Delimiter := ',';
    // List.StrictDelimiter := True;
    // List.DelimitedText := Value;
    List.CommaText := aValue;
    for idx := 0 to List.Count - 1 do
    begin
      Name := List[idx];
      EnumValue := GetEnumValue(EnumInfo, Name);
      if EnumValue >= 0 then
        Include(TIntegerSet(Result), EnumValue);
    end;
  finally
    List.Free;
  end;
end;

var
  AgeCategory: TAgeCategory;
  AgeCategories: TAgeCategories;
  ac: TAgeCategory;
  EnumSource: string;
  EnumSetSource: string;
  EnumValue: Integer;
  EnumFromString: TAgeCategory;
  EnumSetFromString: TAgeCategories;
  SomeIntegerSet: TIntegerSet;
  Enum7Set: TEnum7Set;
  Enum8Set: TEnum8Set;
  Enum9Set: TEnum9Set;
  Enum15Set: TEnum15Set;
  Enum16Set: TEnum16Set;
  Enum17Set: TEnum17Set;
begin
  AgeCategory := acOldHat;
  AgeCategories := [acGreenLeaf, acFairMaiden];
  EnumSource := 'acGreenLeaf';
  EnumSetSource := 'acLongInTheTooth,acOldHat';

  WriteLn;
  WriteLn('Enum (set) --> string');

  WriteLn;
  WriteLn('AgeCategory variable');
  WriteLn(Format('- Ordinal value: %d, Name: %16s', [Ord(AgeCategory), EnumToString(TypeInfo(TAgeCategory), Ord(AgeCategory))]));

  WriteLn;
  WriteLn('TAgeCategory members');
  for ac := Low(TAgeCategory) to High(TAgeCategory) do
    WriteLn(Format('- Ordinal value: %d, Name: %16s', [Ord(ac), EnumToString(TypeInfo(TAgeCategory), Ord(ac))]));

  WriteLn;
  WriteLn('AgeCategories variable');
  for ac := Low(TAgeCategory) to High(TAgeCategory) do
    WriteLn(Format('- Ordinal value: %d, Name: %16s, member included in variable: %3s',
      [Ord(ac), EnumToString(TypeInfo(TAgeCategory), Ord(ac)), BOOLSTR[ac in AgeCategories]]));

  WriteLn;
  WriteLn('AgeCategories variable as single value');
  WriteLn(Format('- Ordinal value: %d, as string: %s', [Byte(AgeCategories), EnumSetToString(TypeInfo(TAgeCategories), Byte(AgeCategories))]));

  WriteLn;
  WriteLn('---------------------------------------------------------------------');

  WriteLn;
  WriteLn('string --> Enum (set)');

  WriteLn;
  WriteLn('Enum source');
  EnumValue := StringToEnum(TypeInfo(TAgeCategory), EnumSource);
  if EnumValue >= 0 then
  begin
    EnumFromString := TAgeCategory(EnumValue);
    WriteLn(Format('Source: %s, Ordinal value: %d, Name: %s', [EnumSource, Ord(EnumFromString), EnumToString(TypeInfo(TAgeCategory), Ord(EnumFromString))]));
  end
  else
    WriteLn(Format('Source: %s does not match any member names of the enumeration type', [EnumSource]));

  WriteLn;
  WriteLn('Enum Set source');
//  Integer(SomeIntegerSet) := Byte(AgeCategories);
//  SomeIntegerSet := TIntegerSet(StringToEnumSet(TypeInfo(TAgeCategories), EnumSetSource));
  Byte(EnumSetFromString) := StringToEnumSet(TypeInfo(TAgeCategories), EnumSetSource);
  WriteLn(Format('Source: %s, Ordinal value: %d, Names: %s', [EnumSetSource, Byte(EnumSetFromString), EnumSetToString(TypeInfo(TAgeCategories), Byte(EnumSetFromString))]));

  WriteLn;
  WriteLn('---------------------------------------------------------------------');

  WriteLn;
  WriteLn('Byte, Word or Integer cast');

  WriteLn;
  WriteLn('SizeOf TEnum7Set: ', SizeOf(TEnum7Set));
  WriteLn('SizeOf TEnum8Set: ', SizeOf(TEnum8Set));
  WriteLn('SizeOf TEnum9Set: ', SizeOf(TEnum9Set));
  WriteLn('SizeOf TEnum15Set: ', SizeOf(TEnum15Set));
  WriteLn('SizeOf TEnum16Set: ', SizeOf(TEnum16Set));
  WriteLn('SizeOf TEnum17Set: ', SizeOf(TEnum17Set));

  Integer(SomeIntegerSet) := Byte(Enum7Set);
  Integer(SomeIntegerSet) := Byte(Enum8Set);
  Integer(SomeIntegerSet) := Word(Enum9Set);
  Integer(SomeIntegerSet) := Word(Enum15Set);
  Integer(SomeIntegerSet) := Word(Enum16Set);
  Integer(SomeIntegerSet) := Integer(Enum17Set);

  ReadLn;

end.
