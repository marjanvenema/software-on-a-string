program ConvertingEnumsAndEnumsetsToAndFromInteger;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  TypInfo;

type
  TMyEnum = (meOne, meTwo, meThree);

procedure ConvertEnumToInteger;
var
  MyEnum: TMyEnum;
  MyInteger: Integer;
begin
  MyEnum := meTwo;
  MyInteger := Ord(MyEnum);
  WriteLn('The Integer equivalent of meTwo is: ', MyInteger);
end;

procedure ConvertIntegerToEnum;
var
  MyEnum: TMyEnum;
  MyInteger: Integer;
begin
  MyInteger := 2;
  MyEnum := TMyEnum(MyInteger);
  WriteLn('The TMyEnum equivalent of 2 is: ', GetEnumName(TypeInfo(TMyEnum), Ord(MyEnum)));
end;

procedure ConvertEnumSetToInteger;
begin
end;

procedure ConvertIntegerToEnumSet;
begin
end;

procedure CastingEnumsProperly;
begin
end;

begin
  ConvertEnumToInteger;
  ConvertIntegerToEnum;

  ConvertEnumSetToInteger;
  ConvertIntegerToEnumSet;

  CastingEnumsProperly;
end.
