program ConvertingEnumsToAndFromStrings;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.Rtti,
  System.SysUtils,
  System.TypInfo;

type
  TAccountType = (atBasic, atDual, atBusiness);
  TMonsterType = (mtGravatar, mtWavatar, mtMysteryMan, mtRetro, mtIdenticon);

(** )
type
  TEnum = class(TObject)
  public
    // Use reintroduce to prevent compiler complaint about hiding virtual method from TObject.
    // Breaking polymorphism like this is no problem here, because we don't need/use polymorphism.
    class function ToString<T>(const aEnumValue: T): string; reintroduce;
    class function FromString<T>(const aEnumString: string; const aDefault: T): T;
  end;

class function TEnum.ToString<T>(const aEnumValue: T): string;
begin
  Assert(PTypeInfo(TypeInfo(T)).Kind = tkEnumeration, 'Type parameter must be an enumeration');

  Result := GetEnumName(TypeInfo(T), TValue.From<T>(aEnumValue).AsOrdinal);
end;

class function TEnum.FromString<T>(const aEnumString: string; const aDefault: T): T;
var
  OrdValue: Integer;
begin
  Assert(PTypeInfo(TypeInfo(T)).Kind = tkEnumeration, 'Type parameter must be an enumeration');

  OrdValue := GetEnumValue(TypeInfo(T), aEnumString);
  if OrdValue < 0 then
    Result := aDefault
  else
    Result := TValue.FromOrdinal(TypeInfo(T), OrdValue).AsType<T>;
end;

procedure RunExample;
var
  AccountType: TAccountType;
  MonsterType: TMonsterType;
  StringValue: string;
begin
    // ToString

    AccountType := atDual;
    WriteLn(Format('AccountType atDual to string: %s', [TEnum.ToString<TAccountType>(AccountType)]));

    MonsterType := mtWavatar;
    WriteLn(Format('MonsterType mtWavatar to string: %s', [TEnum.ToString<TMonsterType>(MonsterType)]));

    // FromString with a known value

    StringValue := 'atBusiness';
    AccountType := TEnum.FromString<TAccountType>(StringValue, atDual);
    WriteLn(Format('AccountType from string "atBusiness" with atDual as default: %d (%s)', [Ord(AccountType), TEnum.ToString<TAccountType>(AccountType)]));

    StringValue := 'mtMysteryMan';
    MonsterType := TEnum.FromString<TMonsterType>(StringValue, mtIdenticon);
    WriteLn(Format('MonsterType from string "mtMysteryMan" with mtIdenticon as default: %d (%s)', [Ord(MonsterType), TEnum.ToString<TMonsterType>(MonsterType)]));

    // FromString with an unknown value

    StringValue := 'Unknown';
    AccountType := TEnum.FromString<TAccountType>(StringValue, atDual);
    WriteLn(Format('AccountType from string "Unknown" with atDual as default: %d (%s)', [Ord(AccountType), TEnum.ToString<TAccountType>(AccountType)]));

    StringValue := 'Unknown';
    MonsterType := TEnum.FromString<TMonsterType>(StringValue, mtIdenticon);
    WriteLn(Format('MonsterType from string "Unknown" with mtIdenticon as default: %d (%s)', [Ord(MonsterType), TEnum.ToString<TMonsterType>(MonsterType)]));

    // And this produces a compilation error as the compiler knows that atDual is not a member of TMonsterType:
    //MonsterType := TEnum.FromString<TMonsterType>(StringValue, atDual);

    // Just like this because the compiler knows that FromString<TMonsterType> does not return a TAccountType:
    //AccountType := TEnum.FromString<TMonsterType>(StringValue, mtMysteryMan);
end;
*)

type
  TEnum<T> = class(TObject)
  public
    // Use reintroduce to prevent compiler complaint about hiding virtual method from TObject.
    // Breaking polymorphism like this is no problem here, because we don't need/use polymorphism.
    class function ToString(const aEnumValue: T): string; reintroduce;
    class function FromString(const aEnumString: string; const aDefault: T): T;
  end;

class function TEnum<T>.ToString(const aEnumValue: T): string;
begin
  Assert(PTypeInfo(TypeInfo(T)).Kind = tkEnumeration, 'Type parameter must be an enumeration');

  Result := GetEnumName(TypeInfo(T), TValue.From<T>(aEnumValue).AsOrdinal);
end;

class function TEnum<T>.FromString(const aEnumString: string; const aDefault: T): T;
var
  OrdValue: Integer;
begin
  Assert(PTypeInfo(TypeInfo(T)).Kind = tkEnumeration, 'Type parameter must be an enumeration');

  OrdValue := GetEnumValue(TypeInfo(T), aEnumString);
  if OrdValue < 0 then
    Result := aDefault
  else
    Result := TValue.FromOrdinal(TypeInfo(T), OrdValue).AsType<T>;
end;

procedure RunExample;
var
  AccountType: TAccountType;
  MonsterType: TMonsterType;
  StringValue: string;
begin
    // ToString

    AccountType := atDual;
    WriteLn(Format('AccountType atDual to string: %s', [TEnum<TAccountType>.ToString(AccountType)]));

    MonsterType := mtWavatar;
    WriteLn(Format('MonsterType mtWavatar to string: %s', [TEnum<TMonsterType>.ToString(MonsterType)]));

    // FromString with a known value

    StringValue := 'atBusiness';
    AccountType := TEnum<TAccountType>.FromString(StringValue, atDual);
    WriteLn(Format('AccountType from string "atBusiness" with atDual as default: %d (%s)', [Ord(AccountType), TEnum<TAccountType>.ToString(AccountType)]));

    StringValue := 'mtMysteryMan';
    MonsterType := TEnum<TMonsterType>.FromString(StringValue, mtIdenticon);
    WriteLn(Format('MonsterType from string "mtMysteryMan" with mtIdenticon as default: %d (%s)', [Ord(MonsterType), TEnum<TMonsterType>.ToString(MonsterType)]));

    // FromString with an unknown value

    StringValue := 'Unknown';
    AccountType := TEnum<TAccountType>.FromString(StringValue, atDual);
    WriteLn(Format('AccountType from string "Unknown" with atDual as default: %d (%s)', [Ord(AccountType), TEnum<TAccountType>.ToString(AccountType)]));

    StringValue := 'Unknown';
    MonsterType := TEnum<TMonsterType>.FromString(StringValue, mtIdenticon);
    WriteLn(Format('MonsterType from string "Unknown" with mtIdenticon as default: %d (%s)', [Ord(MonsterType), TEnum<TMonsterType>.ToString(MonsterType)]));

    // And this produces a compilation error as the compiler knows that atDual is not a member of TMonsterType:
    //MonsterType := TEnum<TMonsterType>.FromString(StringValue, atDual);

    // Just like this because the compiler knows that FromString<TMonsterType> does not return a TAccountType:
    //AccountType := TEnum<TMonsterType>.FromString(StringValue, mtMysteryMan);
end;

begin
  try
    RunExample;
    ReadLn;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
