unit MyImplementerOfSomeInterfaceTestsNonGeneric;

interface

uses
  SomeInterfaceContractTestsNonGeneric;

type
  TMyImplementerOfSomeInterfaceTests = class(TSomeInterfaceContractTests)
  strict protected
    function GetImplementingClass: TInterfacedClass; override;
  end;

implementation

uses
  MyImplementerOfSomeInterface,
  TestFramework;

{ TMyImplementerOfSomeInterfaceTests }

function TMyImplementerOfSomeInterfaceTests.GetImplementingClass: TInterfacedClass;
begin
  Result := TMyImplementerOfSomeInterface;
end;

initialization
  RegisterTest(TMyImplementerOfSomeInterfaceTests.Suite);
end.
