unit SomeInterface;

interface

type
  ISomeInterface = interface(IInterface)
  ['{55F596F3-095A-49E3-A522-F0FCCC3AA57F}']
    function SomeMethod(const aInput: string): Boolean;
  end;

implementation

end.
