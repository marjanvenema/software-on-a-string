unit SomeInterfaceContractTestsNonGeneric;

interface

uses
  SomeInterface,
  TestFramework;

type
  TSomeInterfaceContractTests = class(TTestCase)
  strict private
    FInterfaceUnderTest: ISomeInterface;
  strict protected
    function GetImplementingClass: TInterfacedClass; virtual; abstract;
  protected
    procedure SetUp; override;
  published
    procedure SomeMethod_Input_EmptyString_ShouldReturn_False;
  end;

implementation

uses
  System.SysUtils;

{ TSomeInterfaceContactTests }

procedure TSomeInterfaceContractTests.SetUp;
begin
  inherited;
  Supports(GetImplementingClass.Create, ISomeInterface, FInterfaceUnderTest);
  Assert(Assigned(FInterfaceUnderTest), 'Implementing class does not support ISomeInterface');
end;

procedure TSomeInterfaceContractTests.SomeMethod_Input_EmptyString_ShouldReturn_False;
begin
  CheckEquals(FInterfaceUnderTest.SomeMethod(''), False);
end;

end.
