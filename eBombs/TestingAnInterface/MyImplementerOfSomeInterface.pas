unit MyImplementerOfSomeInterface;

interface

uses
  SomeInterface;

type
  TMyImplementerOfSomeInterface = class(TInterfacedObject, ISomeInterface)
  public
    function SomeMethod(const aInput: string): Boolean;
  end;

implementation

{ TMyImplementerOfSomeInterface }

function TMyImplementerOfSomeInterface.SomeMethod(
  const aInput: string): Boolean;
begin
  Result := True;
end;

end.
