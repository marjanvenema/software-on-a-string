unit SomeInterfaceContractTests;

interface

uses
  TestFramework,
  SomeInterface;

type
  TSomeInterfaceContractTests<T: TInterfacedObject, constructor> = class(TTestCase)
  strict private
    FInterfaceUnderTest: ISomeInterface;
  protected
    procedure SetUp; override;
  published
    procedure SomeMethod_Input_EmptyString_ShouldReturn_False;
  end;

implementation

uses
  System.SysUtils;

{ TSomeInterfaceContractTests<T> }

procedure TSomeInterfaceContractTests<T>.SetUp;
begin
  inherited;
  Supports(T.Create, ISomeInterface, FInterfaceUnderTest);
  Assert(Assigned(FInterfaceUnderTest), 'Generic type does not support ISomeInterface');
end;

procedure TSomeInterfaceContractTests<T>.SomeMethod_Input_EmptyString_ShouldReturn_False;
begin
  CheckEquals(FInterfaceUnderTest.SomeMethod(''), False);
end;

end.
