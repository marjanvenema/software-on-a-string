unit MyImplementerOfSomeInterfaceTests;

interface

uses
  MyImplementerOfSomeInterface,
  SomeInterfaceContractTests;

//type
//  TMyImplementerOfSomeInterfaceTests = class(TSomeInterfaceContractTests<TMyImplementerOfSomeInterface>)
//  end;

implementation

uses
  TestFramework;

initialization
//  RegisterTest(TMyImplementerOfSomeInterfaceTests.Suite);

  RegisterTest(TSomeInterfaceContractTests<TMyImplementerOfSomeInterface>.Suite);

end.
