program NameValuePairsInComboBox;

uses
  Vcl.Forms,
  Better_fm in 'Better_fm.pas' {Better_Form},
  Naive_fm in 'Naive_fm.pas' {Naive_Form},
  AnyValue_fm in 'AnyValue_fm.pas' {AnyValue_Form},
  Gold_fm in 'Gold_fm.pas' {Gold_Form};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TGold_Form, Gold_Form);
  Application.CreateForm(TAnyValue_Form, AnyValue_Form);
  Application.CreateForm(TBetter_Form, Better_Form);
  Application.CreateForm(TNaive_Form, Naive_Form);
  Application.Run;
end.
