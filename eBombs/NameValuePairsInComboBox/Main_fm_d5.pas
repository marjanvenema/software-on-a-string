unit Main_fm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TMain_Form = class(TForm)
    NameValuePairs_Combo: TComboBox;
    Ini_Memo: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FMyNameValuePairs: TStrings;
  public
  end;

var
  Main_Form: TMain_Form;

implementation

{$R *.DFM}

procedure TMain_Form.FormCreate(Sender: TObject);
begin
  FMyNameValuePairs := TStringList.Create;
  FMyNameValuePairs.Add('Jansen=100');
  FMyNameValuePairs.Add('Petersen=200');
  FMyNameValuePairs.Add('Gerritsen=300');
  FMyNameValuePairs.Add('Dirksen=400');
  FMyNameValuePairs.Add('Karelsen=500');

  Ini_Memo.Text := FMyNameValuePairs.Text;
  NameValuePairs_Combo.Items := FMyNameValuePairs; // Does een Assign!
end;

procedure TMain_Form.FormDestroy(Sender: TObject);
begin
  FMyNameValuePairs.Free;
end;

end.
