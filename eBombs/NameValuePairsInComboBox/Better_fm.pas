unit Better_fm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TBetter_Form = class(TForm)
    Memo1: TMemo;
    ComboBox1: TComboBox;
    Name_Text: TStaticText;
    Value_Text: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
  private
    FMyNameValuePairs: TStrings;
  public
  end;

var
  Better_Form: TBetter_Form;

implementation

{$R *.dfm}

procedure TBetter_Form.ComboBox1Select(Sender: TObject);
begin
  Name_Text.Caption := ComboBox1.Items[ComboBox1.ItemIndex];
  // Integer cast of TObject in this case is safe even on 64-bit because we
  // ourselves are putting in Integer values.
  Value_Text.Caption := IntToStr(Integer(ComboBox1.Items.Objects[ComboBox1.ItemIndex]));
end;

procedure TBetter_Form.FormCreate(Sender: TObject);
var
  idx: Integer;
begin
  // Simulate loading from some configuration file
  // OwnsObjects = False is the default, but we state it explicitly here because
  // we are not really storing object instances, but are (ab)using each Item's
  // Object property to hold the integer value from the configuration file.
  FMyNameValuePairs := TStringList.Create({OwnsObjects}False);
  FMyNameValuePairs.Add('Jansen=100');
  FMyNameValuePairs.Add('Petersen=200');
  FMyNameValuePairs.Add('Gerritsen=300');
  FMyNameValuePairs.Add('Dirksen=400');
  FMyNameValuePairs.Add('Karelsen=500');

  // Show the contents of the "configuration" in a memo
  Memo1.Text := FMyNameValuePairs.Text;

  // Load the "configuration" contents of the string list into the combo box
  for idx := 0 to FMyNameValuePairs.Count - 1 do
    ComboBox1.AddItem(FMyNameValuePairs.Names[idx], TObject(StrToIntDef(FMyNameValuePairs.ValueFromIndex[idx], 0)));
end;

procedure TBetter_Form.FormDestroy(Sender: TObject);
begin
  // Free the "configuration" string list.
  // Despite OwnsObjects being False, we should NOT free the Objects
  // ourselves. After all, the Objects don't hold instances, but "just"
  // integers and "freeing" those is asking for trouble.
  FMyNameValuePairs.Free;
end;

end.
