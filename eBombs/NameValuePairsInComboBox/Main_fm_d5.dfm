object Main_Form: TMain_Form
  Left = 192
  Top = 124
  Width = 512
  Height = 287
  Caption = 'Name Value Pairs ComboBox'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object NameValuePairs_Combo: TComboBox
    Left = 256
    Top = 8
    Width = 233
    Height = 21
    ItemHeight = 13
    TabOrder = 0
  end
  object Ini_Memo: TMemo
    Left = 8
    Top = 8
    Width = 233
    Height = 233
    TabOrder = 1
  end
end
