object AnyValue_Form: TAnyValue_Form
  Left = 0
  Top = 0
  Caption = 'Any Value - Name Value Pairs ComboBox'
  ClientHeight = 197
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 217
    Height = 177
    TabOrder = 0
  end
  object ComboBox1: TComboBox
    Left = 240
    Top = 8
    Width = 217
    Height = 21
    TabOrder = 1
    OnSelect = ComboBox1Select
  end
  object Name_Text: TStaticText
    Left = 240
    Top = 145
    Width = 217
    Height = 17
    AutoSize = False
    TabOrder = 2
  end
  object Value_Text: TStaticText
    Left = 240
    Top = 168
    Width = 217
    Height = 17
    AutoSize = False
    TabOrder = 3
  end
end
