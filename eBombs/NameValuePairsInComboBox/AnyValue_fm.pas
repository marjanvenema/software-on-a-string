unit AnyValue_fm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TAnyValue_Form = class(TForm)
    Memo1: TMemo;
    ComboBox1: TComboBox;
    Name_Text: TStaticText;
    Value_Text: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
  private
    FMyNameValuePairs: TStrings;
  public
  end;

var
  AnyValue_Form: TAnyValue_Form;

implementation

{$R *.dfm}

uses
  System.Rtti;

type
  TValueObject = class(TObject)
  strict private
    FValue: TValue;
  public
    constructor Create(const aValue: TValue);
    property Value: TValue read FValue;
  end;

{ TValueObject }

constructor TValueObject.Create(const aValue: TValue);
begin
  FValue := aValue;
end;

procedure TAnyValue_Form.ComboBox1Select(Sender: TObject);
begin
  Name_Text.Caption := ComboBox1.Items[ComboBox1.ItemIndex];
  Value_Text.Caption := TValueObject(ComboBox1.Items.Objects[ComboBox1.ItemIndex]).Value.AsString;
end;

procedure TAnyValue_Form.FormCreate(Sender: TObject);
var
  idx: Integer;
begin
  // Simulate loading from some configuration file
  // We set OwnsObjects to True so that any object instances we add will be
  // freed automatically when we free the string list.
  FMyNameValuePairs := TStringList.Create({OwnsObjects}True);
  FMyNameValuePairs.Add('Jansen=Aquarius');
  FMyNameValuePairs.Add('Petersen=Capricorn');
  FMyNameValuePairs.Add('Gerritsen=Pisces');
  FMyNameValuePairs.Add('Dirksen=Libra');
  FMyNameValuePairs.Add('Karelsen=Scorpio');

  // Show the contents of the "configuration" in a memo
  Memo1.Text := FMyNameValuePairs.Text;

  for idx := 0 to FMyNameValuePairs.Count - 1 do
    FMyNameValuePairs.Objects[idx] :=
      TValueObject.Create(FMyNameValuePairs.ValueFromIndex[idx]);

  for idx := 0 to FMyNameValuePairs.Count - 1 do
    ComboBox1.AddItem(FMyNameValuePairs.Names[idx], FMyNameValuePairs.Objects[idx]);
end;

procedure TAnyValue_Form.FormDestroy(Sender: TObject);
begin
  // Ensure the ComboBox can no longer reference the items in FMyNameValuePairs
  ComboBox1.Clear;

  // Free the "configuration" string list.
  // As OwnsObjects is True, we don't need to free the Objects ourselves.
  // It is done for us.
  FMyNameValuePairs.Free;
end;

end.
