unit Gold_fm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TGold_Form = class(TForm)
    Memo1: TMemo;
    ComboBox1: TComboBox;
    Name_Text: TStaticText;
    Value_Text: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
  private
    FMyNameValuePairs: TStrings;
  public
  end;

var
  Gold_Form: TGold_Form;

implementation

{$R *.dfm}

uses
  System.Rtti;

type
  TValueObject = class(TObject)
  strict private
    FValue: TValue;
  public
    constructor Create(const aValue: TValue);
    property Value: TValue read FValue;
  end;

{ TValueObject }

constructor TValueObject.Create(const aValue: TValue);
begin
  FValue := aValue;
end;

procedure TGold_Form.ComboBox1Select(Sender: TObject);
begin
  Name_Text.Caption := ComboBox1.Items[ComboBox1.ItemIndex];
  Value_Text.Caption := TValueObject(ComboBox1.Items.Objects[ComboBox1.ItemIndex]).Value.AsString;
end;

procedure TGold_Form.FormCreate(Sender: TObject);
var
  idx: Integer;
begin
  // Simulate loading from some configuration file
  // We set OwnsObjects to True so that any object instances we add will be
  // freed automatically when we free the string list.
  FMyNameValuePairs := TStringList.Create({OwnsObjects}True);
  FMyNameValuePairs.Add('Jansen=Aquarius');
  FMyNameValuePairs.Add('Petersen=Capricorn');
  FMyNameValuePairs.Add('Gerritsen=Pisces');
  FMyNameValuePairs.Add('Dirksen=Libra');
  FMyNameValuePairs.Add('Karelsen=Scorpio');

  // Convert the contents so both the ComboBox and Memo can show just the names
  // and the values are still associated with their items using actual object
  // instances.
  for idx := 0 to FMyNameValuePairs.Count - 1 do
  begin
    FMyNameValuePairs.Objects[idx] :=
      TValueObject.Create(FMyNameValuePairs.ValueFromIndex[idx]);

    FMyNameValuePairs.Strings[idx] := FMyNameValuePairs.Names[idx];
  end;

  // Show the contents of the "configuration" in a memo
  Memo1.Text := FMyNameValuePairs.Text;

  // Load the "configuration" contents of the string list into the combo box
  ComboBox1.Items := FMyNameValuePairs; // Does an Assign!
end;

procedure TGold_Form.FormDestroy(Sender: TObject);
begin
  // Ensure the ComboBox can no longer reference the items in FMyNameValuePairs
  ComboBox1.Clear;

  // Free the "configuration" string list.
  // As OwnsObjects is True, we don't need to free the Objects ourselves.
  // It is done for us.
  FMyNameValuePairs.Free;
end;

end.
