unit Naive_fm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TNaive_Form = class(TForm)
    Memo1: TMemo;
    ComboBox1: TComboBox;
    Value_Text: TStaticText;
    Name_Text: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
  private
    FMyNameValuePairs: TStrings;
  public
  end;

var
  Naive_Form: TNaive_Form;

implementation

{$R *.dfm}

procedure TNaive_Form.ComboBox1Select(Sender: TObject);
begin
  Value_Text.Caption := ComboBox1.Items[ComboBox1.ItemIndex];
end;

procedure TNaive_Form.FormCreate(Sender: TObject);
begin
  // Simulate loading from some configuration file
  FMyNameValuePairs := TStringList.Create;
  FMyNameValuePairs.Add('Jansen=100');
  FMyNameValuePairs.Add('Petersen=200');
  FMyNameValuePairs.Add('Gerritsen=300');
  FMyNameValuePairs.Add('Dirksen=400');
  FMyNameValuePairs.Add('Karelsen=500');

  // Show the contents of the "configuration" in a memo
  Memo1.Text := FMyNameValuePairs.Text;

  // Assign the "configuration" contents of the string list to the combo box
  ComboBox1.Items := FMyNameValuePairs; // Does an Assign!
end;

procedure TNaive_Form.FormDestroy(Sender: TObject);
begin
  // Free the "configuration" string list.
  FMyNameValuePairs.Free;
end;

end.
