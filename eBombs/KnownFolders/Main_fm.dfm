object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 337
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    635
    337)
  PixelsPerInch = 96
  TextHeight = 13
  object SaveButton: TButton
    Left = 391
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 0
    OnClick = SaveButtonClick
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 377
    Height = 321
    Anchors = [akLeft, akTop, akBottom]
    TabOrder = 1
  end
  object InitialDirEdit: TEdit
    Left = 391
    Top = 128
    Width = 236
    Height = 21
    TabOrder = 2
  end
  object SaveDialog1: TSaveDialog
    Left = 416
    Top = 40
  end
end
