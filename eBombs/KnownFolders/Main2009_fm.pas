unit Main2009_fm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  ShellApi, ShlObj, FolderHelper;

procedure TForm1.FormCreate(Sender: TObject);
var
  FolderPath: array[0 .. MAX_PATH] of Char;
begin
//  function SHGetSpecialFolderPath(hwndOwner: HWND; lpszPath: PWideChar;
//  nFolder: Integer; fCreate: BOOL): BOOL; stdcall;

  SHGetSpecialFolderPath(0, @FolderPath, CSIDL_PERSONAL, False);

  Memo1.Lines.Add('CSIDL_PERSONAL: ' + FolderPath);
  Memo1.Lines.Add('My Documents RFolderHelper: ' + RFolderHelper.GetMyDocumentsFolder);
  Memo1.Lines.Add('My Music RFolderHelper: ' + RFolderHelper.GetMyMusicFolder);
end;

end.
