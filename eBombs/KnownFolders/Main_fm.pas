unit Main_fm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  System.IOUtils;

type
  TMainForm = class(TForm)
    SaveDialog1: TSaveDialog;
    SaveButton: TButton;
    Memo1: TMemo;
    InitialDirEdit: TEdit;
    procedure SaveButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  Winapi.SHFolder;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Add('Current dir: ' + TDirectory.GetCurrentDirectory);
  Memo1.Lines.Add('Home path (%USERPROFILE%): ' + TPath.GetHomePath);
  Memo1.Lines.Add('Documents (My Documents): ' + TPath.GetDocumentsPath);
  Memo1.Lines.Add('Pictures (My Pictures): ' + TPath.GetPicturesPath);
  Memo1.Lines.Add('Music (My Music): ' + TPath.GetMusicPath);
  Memo1.Lines.Add('Movies (My Videos): ' + TPath.GetMoviesPath);
  Memo1.Lines.Add('Temporary files (TEMP): ' + TPath.GetTempPath);
end;

procedure TMainForm.SaveButtonClick(Sender: TObject);
begin
//  SaveDialog1.InitialDir := InitialDirEdit.Text;
//  SaveDialog1.Execute();
//  InitialDirEdit.Text := ExcludeTrailingPathDelimiter(ExtractFilePath(SaveDialog1.FileName));

  if InitialDirEdit.Text = '' then
    SaveDialog1.InitialDir := IncludeTrailingPathDelimiter(TPath.GetDocumentsPath) + 'Your Beautiful App'
  else
    SaveDialog1.InitialDir := InitialDirEdit.Text;

  if SaveDialog1.Execute() then
    InitialDirEdit.Text := ExcludeTrailingPathDelimiter(ExtractFilePath(SaveDialog1.FileName));
end;

end.
