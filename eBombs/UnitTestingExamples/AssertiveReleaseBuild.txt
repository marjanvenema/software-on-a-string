Release build

Begin

Before assert that should Pass - using Debug
- This _should_ be seen.
After assert that should Pass - using Debug

Before assert that should Fail - using Debug
- This should _not_ be seen.
After assert that should Fail - using Debug

Before assert that should Pass - using Trace
- This _should_ be seen.
After assert that should Pass - using Trace

Before assert that should Fail - using Trace
- Assert condition failed.
- This should _not_ be seen.
After assert that should Fail - using Trace

Before assert that should Pass - using DebugEx
- This _should_ be seen.
After assert that should Pass - using DebugEx

Before assert that should Fail - using DebugEx
- This should _not_ be seen.
After assert that should Fail - using DebugEx

Before assert that should Pass - using TraceEx
- This _should_ be seen.
After assert that should Pass - using TraceEx

Before assert that should Fail - using TraceEx
- AssertionFailure caught.
After assert that should Fail - using TraceEx

End
