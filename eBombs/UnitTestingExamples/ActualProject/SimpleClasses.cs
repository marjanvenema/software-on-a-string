﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject
{
    class SimpleClass
    {
        public SimpleResult SimplePassThrough(string someString, int someInteger)
        {
            SimpleResult result = MakeResult();
            RealWork work = MakeWork();

            if (!work.DoActualWork(someString, someInteger))
            {
                result.Error = work.Error;
            }
            return result;
        }

        virtual protected SimpleResult MakeResult()
        {
            return new SimpleResult();
        }

        virtual protected RealWork MakeWork()
        {
            return new RealWork();
        }
    }

    class SimpleResult
    {
        public int Error { get; set; }
    }

    class RealWork
    {
        public int Error { get; protected set; }

        virtual public Boolean DoActualWork(string aString, int aInt)
        {
            return false;
        }
    }
}
