﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.TestingAbstractBaseClasses
{
    abstract class Bashful
    {
        protected abstract string LetDescendantComposeFinalResult(string textToUseInComposition);
        protected abstract string LetDescendantProvideDefault();

        public string DoSomethingUseful(string textToUseInComposition)
        {
            if (string.IsNullOrEmpty(textToUseInComposition))
            {
                return LetDescendantComposeFinalResult(LetDescendantProvideDefault());
            }
            else
            {
                return LetDescendantComposeFinalResult(textToUseInComposition);
            }
        }
    }
}
