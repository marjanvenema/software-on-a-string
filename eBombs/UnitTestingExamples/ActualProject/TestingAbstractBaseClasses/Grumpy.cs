﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.TestingAbstractBaseClasses
{
    class Grumpy : Bashful
    {
        protected override string LetDescendantComposeFinalResult(string textToUseInComposition)
        {
            return "";
        }

        protected override string LetDescendantProvideDefault()
        {
            return "";
        }
    }
}
