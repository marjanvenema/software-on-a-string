﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.CanHandleFromHandle
{
    interface IWork
    {

    }

    interface IWorker
    {
        Boolean CanHandle(IWork work);
        void Handle(IWork work);
    }

    class IndecentProposalError : Exception { };
}
