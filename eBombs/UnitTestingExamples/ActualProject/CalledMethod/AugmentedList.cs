﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.CalledMethod
{
    class AugmentedListEmptyStringError : Exception
    {
        public AugmentedListEmptyStringError() : base("Cannot add an empty string.") { }
    };
    class AugmentedListDuplicateStringError : Exception
    {
        public AugmentedListDuplicateStringError(string duplicateString, int foundAtIndex) :
            base(String.Format("Cannot add {0}. It is already present at index {1}.", duplicateString, foundAtIndex)) { }
    }
    class AugmentedListNegativeIndexError : Exception 
    {
        public AugmentedListNegativeIndexError() : base("Index can not have a negative value.") { }
    }
    class AugmentedListIndexTooHighError : Exception
    {
        public AugmentedListIndexTooHighError(int index, int listCount) :
            base(String.Format("Cannot add at index {0}. The list is only {1} items long.", index, listCount)) { }
    }

    class AugmentedList
    {
        private List<string> _List = new List<string>();
        public int Count { get { return _List.Count; } }
        private List<string> _Errors = new List<string>();
        public string ErrorText { get { return string.Concat(_Errors); } }

        public void ValidateInput(string key, int index)
        {
            if (index < 0) throw new AugmentedListNegativeIndexError();
            if (index > _List.Count) throw new AugmentedListIndexTooHighError(index, _List.Count);
            if (string.IsNullOrEmpty(key)) throw new AugmentedListEmptyStringError();
            if (_List.Contains(key)) throw new AugmentedListDuplicateStringError(key, _List.IndexOf(key));
        }

        public void AddKey(string key)
        {
            AddKey(key, _List.Count);
        }

        public void AddKey(string key, int index)
        {
            ValidateInput(key, index);
            _List.Add(key);
        }

        public void MoveKey(string key, int fromIndex, int toIndex)
        {
            ValidateInput(key, fromIndex);
            ValidateInput(key, toIndex);
            _List.Remove(key);
        }

        public void RemoveKey(string key)
        {
            ValidateInput(key, 0);
            _List.Remove(key);
        }

        public void ValidateInputAddingMessage(string key, int index)
        {
            _Errors.Clear();
            if (index < 0) _Errors.Add("Index can not have a negative value.");
            if (index > _List.Count) _Errors.Add(String.Format("Cannot add at index {0}. The list is only {1} items long.", index, _List.Count));
            if (string.IsNullOrEmpty(key)) _Errors.Add("Cannot add an empty string.");
            if (_List.Contains(key)) _Errors.Add(String.Format("Cannot add {0}. It is already present at index {1}.", key, _List.IndexOf(key))); 
        }
    }
}
