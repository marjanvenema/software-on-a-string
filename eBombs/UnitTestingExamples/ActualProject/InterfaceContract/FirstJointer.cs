﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.InterfaceContract
{
    class FirstJointer : IJointer
    {
        public IDoveTailJoint MakeDoveTailJoint(IPieceOfWood woodTails, IPieceOfWood woodPins)
        {
            return new FirstDoveTailJoint();
        }
    }

    class FirstDoveTailJoint : IDoveTailJoint
    {

    }
}
