﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.InterfaceContract
{
    interface IDoveTailJoint {}

    interface IPieceOfWood {}

    interface IJointer
    {
        IDoveTailJoint MakeDoveTailJoint(IPieceOfWood woodTails, IPieceOfWood woodPins);
    }
}
