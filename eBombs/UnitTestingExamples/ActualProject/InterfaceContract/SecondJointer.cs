﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.InterfaceContract
{
    class SecondJointer : IJointer
    {
        public IDoveTailJoint MakeDoveTailJoint(IPieceOfWood woodTails, IPieceOfWood woodPins)
        {
            return new SecondDoveTailJoint();
        }
    }

    class SecondDoveTailJoint : IDoveTailJoint
    {

    }
}
