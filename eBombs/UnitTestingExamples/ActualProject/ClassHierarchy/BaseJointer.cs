﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.ClassHierarchy
{
    class PieceOfWood
    {
        public int Length { get; private set; }
        public int Width { get; private set; }
        public int Thickness { get; private set; }
    }

    class DoveTailJoint
    {
        public int NumberOfTails { get; private set; }
        public int TailWidth { get; private set; }
        public int TailLength { get; private set; }
        public int Angle { get; private set; }
        public int FullPinWidth { get; private set; }
        public int SidePinWidth { get; private set; }

        public DoveTailJoint(int numberTails, int tailWidth, int tailLength, int jointLength)
        {
            NumberOfTails = numberTails;
            TailWidth = tailWidth;
            TailLength = tailLength;
            Angle = 20;
            FullPinWidth = (jointLength - (NumberOfTails * TailWidth)) / (NumberOfTails + 1);
            SidePinWidth = (jointLength - (NumberOfTails * TailWidth) - ((NumberOfTails - 1) * FullPinWidth)) / 2;
        }
    }

    class BaseJointer
    {
        public DoveTailJoint MakeDoveTailJoint(PieceOfWood woodTails, PieceOfWood woodPins)
        {
            int numberTails = DecideNumberOfTails(woodTails, woodPins);
            int tailWidth = DecideTailWidth(woodTails, numberTails);
            int tailLength = DecideTailLength(woodTails, woodPins);

            int fullPins = numberTails - 1;
            /* snip */
            /* http://www.finewoodworking.com/how-to/article/all-about-dovetail-joints.aspx */
            return new DoveTailJoint(numberTails, tailWidth, tailLength, woodTails.Width);
        }

        /*
         * Test list
         * - Should throw ArgumentNullException when woodTails unassigned
         * - Should set ParamName of ArgumentNullException to woodTails when woodTails unassigned
         * - Should throw ArgumentNullException when woodPins unassigned
         * - Should set ParamName of ArgumentNullException to woodPins when woodPins unassigned
         * - Should return non-null reference when no exceptions are thrown
         * - Should return DoveTailJoint with at least 1 tail
         * - Should return DoveTailJoint with non-zero TailWidth
         * - Should return DoveTailJoint with non-zero TailHeight
         * - Should return DoveTailJoint with non-zero FullPinWidth
         * - Should return DoveTailJoint with non-zero SidePinWidth
         * - Should return DoveTailJoint that fits woodTails' width: ((number of tails) * TailWidth) + (((number of tails) - 1) * FullPinWidth) + (2 * SidePinWidth)
         */

        protected virtual int DecideNumberOfTails(PieceOfWood woodTails, PieceOfWood woodPins)
        {
            return 1;
        }

        protected virtual int DecideTailWidth(PieceOfWood woodTails, int numberTails)
        {
            return 1;
        }

        protected virtual int DecideTailLength(PieceOfWood woodTails, PieceOfWood woodPins)
        {
            return 1;
        }
    }
}
