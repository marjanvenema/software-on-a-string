﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.ComparingCollections
{
    class Earth
    {
        private List<Mountain> _ListOfMountains;

        public Earth()
        {
            _DataFile = @"D:\Repositories\SoftwareOnAString\eBombs\UnitTestingExamples\datafile.ini";
            _ListOfMountains = new List<Mountain>();
        }

        public List<Mountain> GetAllMountains()
        {
            RetrieveMountains();
            return _ListOfMountains;
        }

        public void AddMountain(Mountain mountain)
        {
            _ListOfMountains.Add(mountain);
            StoreMountains();
        }

        private void RetrieveMountains()
        {
            string[] mountains = ReadMountains();
            foreach (string namevalue in mountains)
            {
                string[] parts = namevalue.Split('=');
                int altitude = int.Parse(parts[1]);
                _ListOfMountains.Add(new Mountain(altitude, parts[0]));
            }
        }

        private void StoreMountains()
        {
            foreach (var mountain in _ListOfMountains)
            {
                Write("Mountains", mountain.Name, mountain.Altitude.ToString());
            }
        }

        private string _DataFile;

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSectionNames(
            byte[] buffer,
            int size,
            string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(
            string section,
            byte[] buffer,
            int size,
            string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(
            string section,
            string key,
            string def,
            StringBuilder retVal,
            int size,
            string filePath);

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(
            string section,
            string key,
            string val,
            string filePath);

        private string[] ReadSectionNames()
        {
            byte[] buffer = new byte[1024];
            int i = GetPrivateProfileSectionNames(buffer, 1024, _DataFile);
            string s = System.Text.Encoding.Default.GetString(buffer, 0, i - 1); // strip last separating and terminating '\0'
            return s.Split('\0');
        }

        private string[] ReadMountains()
        {
            byte[] buffer = new byte[1024];
            int i = GetPrivateProfileSection("Mountains", buffer, 1024, _DataFile);
            string s = System.Text.Encoding.Default.GetString(buffer, 0, i - 1); // strip last separating and terminating '\0'
            return s.Split('\0');
        }

        private string Read(string section, string key)
        {
            StringBuilder SB = new StringBuilder(255);
            int i = GetPrivateProfileString(section, key, "", SB, 255, _DataFile);
            return SB.ToString();
        }

        private void Write(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value.ToLower(), _DataFile);
        }
    }
}
