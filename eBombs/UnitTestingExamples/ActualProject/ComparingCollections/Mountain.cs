﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActualProject.ComparingCollections
{
    class Mountain
    {
        public int Altitude { get; private set; }
        public string Name { get; private set; }

        public Mountain(int altitude, string name)
        {
            Altitude = altitude;
            Name = name;
        }
    }
}
