﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject.InterfaceContract;

namespace TestProject.InterfaceContract
{
    class PieceOfWood_Fake : IPieceOfWood {}

    [TestClass]
    public abstract class IJointer_Contract_MS
    {
        internal abstract IJointer MakeJointer();

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_No_Exceptions_Raised_Should_Return_Non_Null_DoveTailJoint()
        {
            var jointer = MakeJointer();
            var DoveTailJoint = jointer.MakeDoveTailJoint(new PieceOfWood_Fake(), new PieceOfWood_Fake());
            Assert.IsNotNull(DoveTailJoint);
        }
    }

    [TestClass]
    public class IJointer_FirstJointer : IJointer_Contract_MS 
    {
        internal override IJointer MakeJointer() { return new FirstJointer(); }
    }

    [TestClass]
    public class IJointer_SecondJointer : IJointer_Contract_MS
    {
        internal override IJointer MakeJointer() { return new SecondJointer(); }
    }

    [TestClass]
    class SomeInternalTestClass
    {
        [TestMethod]
        void SomeInternalTestMethod() { }
    }

    [TestClass]
    public class SomePublicTestClass
    {
        [TestMethod]
        void SomeInternalTestMethod() { }

        [TestMethod]
        public void SomePublicTestMethod() { }
    }
}
