﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject.ComparingCollections;
using System.Collections.Generic;

namespace TestProject.ComparingCollections
{
    //[TestClass]
    public class SimpleDataAccessLayer_Tests
    {
        //[TestMethod]
        public void AddingMountain_Should_StoreIt_And_ReturnIt()
        {
            List<Mountain> beforePlusNew = new Earth().GetAllMountains();
            Mountain mountain = new Mountain(8848, "Everest");

            new Earth().AddMountain(mountain);

            // If you want to test that the new mountain was indeed persisted to storage, you should
            // use a new instance so in memory caching doesn't play a role - assuming that the cache
            // is in the data layer classes and would be initialized when creating a new instance.
            // If you want to test only that the new mountain made it into the in memory cache, you can
            // reuse the instance you created to get the "before" list.
            // But note that when reusing the DAL, the CollectionAssert.AreEqual WON'T FAIL because both
            // lists reference the same instances!
            /*List<Mountain> after = DAL.GetAllMountains();*/
            List<Mountain> after = new Earth().GetAllMountains();

            beforePlusNew.Add(mountain);
            CollectionAssert.AreEqual(beforePlusNew, after);
        }

        private class MountainComparer : Comparer<Mountain>
        {
            public override int Compare(Mountain x, Mountain y)
            {
                // compare the two mountains
                // for the purpose of this tests they are considered equal when their identifiers (names) match
                return x.Name.CompareTo(y.Name);
            }
        }

        [TestMethod]
        public void AddingMountain_Should_StoreIt_And_ReturnIt_V2()
        {
            List<Mountain> beforePlusNew = new Earth().GetAllMountains();
            Mountain mountain = new Mountain(8848, "Everest");

            new Earth().AddMountain(mountain);

            // Using a comparer, you won't be fooled by reference (in)equality
            // The test still fails though, because SimpleDataAccessLayer hasn't been implemented properly.
            // Which is good because that is what this test is intended to catch.
            List<Mountain> after = new Earth().GetAllMountains();

            beforePlusNew.Add(mountain);
            CollectionAssert.AreEqual(beforePlusNew, after, new MountainComparer());
        }
    }
}
