﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject;

namespace TestProject
{
    //[TestClass]
    public class SimpleClasses_Tests
    {
        [TestMethod]
        public void SimplePassThrough_ActualWork_ReturnsTrue_ShouldReturn_NoError()
        {
            // Arrange
            SimpleClass cut = new CutDescendant();

            // Act
            SimpleResult result = cut.SimplePassThrough("Ok", 100);

            // Assert
            Assert.AreEqual(0, result.Error);
        }

        [TestMethod]
        public void SimplePassThrough_ActualWork_ReturnsFalse_ShouldReturn_ErrorFromWork()
        {
            // Arrange
            SimpleClass cut = new CutDescendant();

            // Act
            SimpleResult result = cut.SimplePassThrough("Error", 500);

            // Assert
            Assert.AreEqual(550, result.Error);
        }
    }

    class CutDescendant : SimpleClass
    {
        override protected RealWork MakeWork()
        {
            return new RealWorkTestDouble();
        }
    }

    class RealWorkTestDouble : RealWork
    {
        override public Boolean DoActualWork(string aString, int aInt)
        {
            if (aInt != 100)
                Error = 550;

            return aInt == 100;
        }
    }
}
