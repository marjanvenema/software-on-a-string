﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject.ClassHierarchy;

namespace TestProject.ClassHierarchy
{
    [TestClass]
    public class BaseJointer_Tests
    {
        //[TestMethod]
        public void TestMethod1()
        {
            var woodTail = new PieceOfWood();
            var woodPins = new PieceOfWood();
            var jointer = new BaseJointer();
            jointer.MakeDoveTailJoint(null, null);

        }

        internal virtual BaseJointer MakeJointer()
        {
            return new BaseJointer();
        }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_woodTails_Unassigned_Should_Throw_ArgumentNullException() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_woodTails_Unassigned_Should_Set_ParamName_Of_ArgumentNullException() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_woodPins_Unassigned_Should_Throw_ArgumentNullException() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_woodPins_Unassigned_Should_Set_ParamName_Of_ArgumentNullException() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_No_Exceptions_Raised_Should_Return_Non_Null_DoveTailJoint() 
        {
            var jointer = MakeJointer();
            var DoveTailJoint = jointer.MakeDoveTailJoint(new PieceOfWood(), new PieceOfWood());
            Assert.IsNotNull(DoveTailJoint);
        }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_At_Least_1_Tail() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_TailWidth() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_TailLength() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_FullPinWidth() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_SidePinWidth() { }

        [TestMethod]
        [TestCategory("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_That_Fits_woodTails_Width() { }


    }
}
