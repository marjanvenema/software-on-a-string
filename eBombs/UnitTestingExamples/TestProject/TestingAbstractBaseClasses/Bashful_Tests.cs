﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject.TestingAbstractBaseClasses;

namespace TestProject.TestingAbstractBaseClasses
{
    //[TestClass]
    public class Bashful_Tests
    {
        private class Bashful_Tester : Bashful
        {
            protected override string LetDescendantComposeFinalResult(string textToUseInComposition)
            {
                return "ComposedFinalResult:" + textToUseInComposition;
            }

            protected override string LetDescendantProvideDefault()
            {
                return UseAsDefault;
            }

            public string UseAsDefault { get; set; }
        }

        private Bashful _Bashful;

        protected void ArrangeBashfulInstance(string useAsDefault)
        {
            Bashful_Tester tester = new Bashful_Tester();
            tester.UseAsDefault = useAsDefault;
            _Bashful = tester;
        }

        [TestMethod]
        public void DoSomethingUseful_WhenPassedNull_ShouldUseValueProvidedByDescendant()
        {
            ArrangeBashfulInstance("MyDefault");

            string testResult = _Bashful.DoSomethingUseful(null);

            Assert.AreEqual("ComposedFinalResult:MyDefault", testResult);
        }

        [TestMethod]
        public void DoSomethingUseful_WhenPassedEmpty_ShouldUseValueProvidedByDescendant()
        {
            ArrangeBashfulInstance("AnotherDefault");

            string testResult = _Bashful.DoSomethingUseful(string.Empty);

            Assert.AreEqual("ComposedFinalResult:AnotherDefault", testResult);
        }

        [TestMethod]
        public void DoSomethingUseful_WhenPassedExplicitValue_ShouldUsePassedInValue()
        {
            ArrangeBashfulInstance("ShouldBeIgnored");

            string testResult = _Bashful.DoSomethingUseful("ProvidedValue");

            Assert.AreEqual("ComposedFinalResult:ProvidedValue", testResult);
        }
    }
}
