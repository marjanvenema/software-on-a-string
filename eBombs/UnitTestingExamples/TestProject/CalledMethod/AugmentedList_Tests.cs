﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ActualProject.CalledMethod;

namespace TestProject.CalledMethod
{
    [TestClass]
    public class AugmentedList_Tests
    {
        private AugmentedList MakeAugmentedList()
        {
            return new AugmentedList();
        }

        [TestMethod]
        [ExpectedException(typeof(AugmentedListEmptyStringError), "Yo!")]
        public void ValidateInput_EmptyString_ShouldThrow_AugmentedListEmptyStringError()
        {
            var list = MakeAugmentedList();

            list.ValidateInput("", 0);
        }
        [TestMethod]
        [ExpectedException(typeof(AugmentedListDuplicateStringError), "Yo!")]
        public void ValidateInput_DuplicateString_ShouldThrow_AugmentedListDuplicateStringError()
        {
            var list = MakeAugmentedList();
            list.AddKey("Duplicate");

            list.ValidateInput("Duplicate", 0);
        }
        [TestMethod]
        [ExpectedException(typeof(AugmentedListNegativeIndexError), "Yo!")]
        public void ValidateInput_IndexNegative_ShouldThrow_AugmentedListNegativeIndexError()
        {
            var list = MakeAugmentedList();

            list.ValidateInput("SomeKey", -1);
        }
        [TestMethod]
        public void ValidateInput_IndexEqualToCount_ShouldNotThrow_AnyError()
        {
            var list = MakeAugmentedList();

            list.ValidateInput("SomeKey", 0);

            // Assert.
        }
        [TestMethod]
        [ExpectedException(typeof(AugmentedListIndexTooHighError), "Yo!")]
        public void ValidateInput_IndexHigherThanCount_ShouldThrow_AugmentedListIndexTooHighError()
        {
            var list = MakeAugmentedList();

            list.ValidateInput("SomeKey", 1);
        }
        [TestMethod]
        [ExpectedException(typeof(AugmentedListNegativeIndexError), "Yo!")]
        public void AddKey_ShouldCall_ValidateInput()
        {
            var list = MakeAugmentedList();
            list.ValidateInput("Irrelevant", -1);
        }
        [TestMethod]
        public void AddKey_InvalidInput_ShouldNotChange_ContentsOfList()
        {
            var list = MakeAugmentedList();
            var listCopy = MakeAugmentedList();

            try
            {
                list.AddKey("", 0);
            }
            catch
            {
                // Ignore for the purpose of this test
            }

            Assert.AreEqual(string.Concat(listCopy), string.Concat(list));
        }
        [TestMethod]
        public void AddKey_StringAlreadyPresent_ShouldNotChange_NumberOfItems()
        {
            var list = MakeAugmentedList();
            var listCopy = MakeAugmentedList();
            list.AddKey("Duplicate");
            listCopy.AddKey("Duplicate");

            try
            {
                list.AddKey("Duplicate", 0);
            }
            catch
            {
                // Ignore for the purpose of this test
            }

            Assert.AreEqual(listCopy.Count, list.Count);
        }
        [TestMethod]
        public void AddKey_StringNotYetPresent_ShouldIncrease_NumberOfItems()
        {
        }
        [TestMethod]
        public void AddKey_StringNotYetPresent_ShouldReturn_StringAtGivenIndex()
        {
        }

        [TestMethod]
        public void ValidateInputAddingMessage_EmptyString_ShouldCause_NonEmptyErrorText()
        {
            var list = MakeAugmentedList();

            list.ValidateInputAddingMessage("", 0);

            Assert.AreNotEqual(list.ErrorText, "");
        }

    }
}
