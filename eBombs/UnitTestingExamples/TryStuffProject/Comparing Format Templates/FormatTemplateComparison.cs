﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TryStuffProject.Comparing_Format_Templates
{
    class FormatTemplateComparison
    {
        Boolean AreEqual(string template, string text)
        {
            string regexed = template.Replace("{0}", ".*");
            regexed = template.Replace("{1}", ".*");
            regexed = template.Replace("{2}", ".*");

            Regex regex = new Regex(regexed);
            return regex.IsMatch(text);

        }
    }
}
