﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryStuffProject.Assertions
{
    class AssertionFailure : Exception 
    {
        public AssertionFailure() : base() { }
        public AssertionFailure(string message) : base(message) { }
        //public AssertionFailureError(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public AssertionFailure(string message, Exception innerException) : base(message, innerException) { }
    }

    class DebugEx
    {
        [Conditional("DEBUG")]
        public static void Assert(bool condition)
        {
            if (!condition)
                throw new AssertionFailure("Assertion condition failed.");
        }
        
        [Conditional("DEBUG")]
        public static void Assert(Boolean condition, string message)
        {
            if (!condition)
                throw new AssertionFailure(message);
        }

        [Conditional("DEBUG")]
        public static void Assert(bool condition, string message, string detailMessage)
        {
            if (!condition)
                throw new AssertionFailure(string.Join("\n", message, detailMessage));
        }
        
        [Conditional("DEBUG")]
        public static void Assert(bool condition, string message, string detailMessageFormat, params object[] args)
        {
            if (!condition)
                throw new AssertionFailure(string.Join("\n", message, string.Format(detailMessageFormat, args)));
        }
    }

    class TraceEx
    {
        public static void Assert(bool condition)
        {
            if (!condition)
                throw new AssertionFailure("Assertion condition failed.");
        }

        public static void Assert(Boolean condition, string message)
        {
            if (!condition)
                throw new AssertionFailure(message);
        }

        public static void Assert(bool condition, string message, string detailMessage)
        {
            if (!condition)
                throw new AssertionFailure(string.Join("\n", message, detailMessage));
        }

        public static void Assert(bool condition, string message, string detailMessageFormat, params object[] args)
        {
            if (!condition)
                throw new AssertionFailure(string.Join("\n", message, string.Format(detailMessageFormat, args)));
        }
    }
}
