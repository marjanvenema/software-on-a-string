﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryStuffProject.Assertions
{
    enum UseClass
    {
        Debug,
        Trace,
        DebugEx,
        TraceEx
    }

    enum AssertShould
    {
        Pass,
        Fail
    }

    class Assertive
    {
        public static void TryIt(UseClass useClass, AssertShould assertShould)
        {
            Assertive a = new Assertive(useClass);

            Trace.WriteLine(string.Format("Before assert that should {1} - using {0}", useClass, assertShould));

            if (assertShould == AssertShould.Fail)
            {
                try
                {
                    a.Assert(false, "- Assert condition failed.");
                    Trace.WriteLine("- This should _not_ be seen.");
                }
                catch (AssertionFailure)
                {
                    Trace.WriteLine("- AssertionFailure caught.");
                }
            }
            else
            {
                a.Assert(true, "- This should _never_ be seen.");
                Trace.WriteLine("- This _should_ be seen.");
            }

            Trace.WriteLine(string.Format("After assert that should {1} - using {0}", useClass, assertShould));
            Trace.WriteLine("");
        }

        UseClass _UseClass;
        Assertive(UseClass useClass)
        {
            _UseClass = useClass;
        }

        void Assert(Boolean condition, string message)
        {
            if (_UseClass == UseClass.Debug)
                Debug.Assert(condition, message);
            else
                if (_UseClass == UseClass.Trace)
                    Trace.Assert(condition, message);
                else
                    if (_UseClass == UseClass.DebugEx)
                        DebugEx.Assert(condition, message);
                    else
                        TraceEx.Assert(condition, message);
        }
    }
}
