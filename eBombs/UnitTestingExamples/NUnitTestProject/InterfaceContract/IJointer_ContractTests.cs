﻿using System;
using NUnit.Framework;
using ActualProject.InterfaceContract;

namespace NUnitTestProject
{
    class PieceOfWood_Fake : IPieceOfWood
    {

    }

    [TestFixture(typeof(FirstJointer))]
    [TestFixture(typeof(SecondJointer))]
    class IJointer_Contract<T> where T : IJointer, new() // new() is needed to be able to create new instances of T
    {

        IJointer MakeJointer() 
        {
            return new T();
        }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_woodTails_Unassigned_Should_Throw_ArgumentNullException() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_woodTails_Unassigned_Should_Set_ParamName_Of_ArgumentNullException() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_woodPins_Unassigned_Should_Throw_ArgumentNullException() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_woodPins_Unassigned_Should_Set_ParamName_Of_ArgumentNullException() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_No_Exceptions_Raised_Should_Return_Non_Null_DoveTailJoint()
        {
            var jointer = MakeJointer();
            var DoveTailJoint = jointer.MakeDoveTailJoint(new PieceOfWood_Fake(), new PieceOfWood_Fake());
            Assert.IsNotNull(DoveTailJoint);
        }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_At_Least_1_Tail() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_TailWidth() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_TailLength() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_FullPinWidth() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_With_Non_Zero_SidePinWidth() { }

        [Test]
        [Category("Jointer")]
        public void MakeDoveTailJoint_Should_Return_DoveTailJoint_That_Fits_woodTails_Width() { }
    
    }
}
