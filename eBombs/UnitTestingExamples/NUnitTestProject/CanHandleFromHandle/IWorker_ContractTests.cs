﻿using ActualProject.CanHandleFromHandle;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTestProject.CanHandleFromHandle
{
    enum EnsureCanHandle
    {
        ReturnsFalse,
        ReturnsTrue
    }

    class Work_Fake : IWork { }

    class TestWrapper : IWorker
    {
        IWorker _WorkerUnderTest;
        EnsureCanHandle _ReturnsThis;

        public TestWrapper(IWorker workerUnderTest, EnsureCanHandle returnsThis)
        {
            _WorkerUnderTest = workerUnderTest;
            _ReturnsThis = returnsThis;
        }

        public Boolean CanHandle(IWork work)
        {
            return (_ReturnsThis == EnsureCanHandle.ReturnsTrue);
        }

        public void Handle(IWork work)
        {
            _WorkerUnderTest.Handle(work);
        }
    }

    [TestFixture(typeof(KitchenWorker))]
    [TestFixture(typeof(BathroomWorker))]
    class IWorker_ContractTests<T> where T : IWorker, new()
    {
        IWorker MakeWorker(EnsureCanHandle returnsThis)
        {

            return new TestWrapper(new T(), returnsThis);
        }

        [Test]
        public void Handle_CanHandle_Returns_False_Should_Throw_IndecentProposalError()
        {
            IWorker worker = MakeWorker(EnsureCanHandle.ReturnsFalse);
            IWork work = new Work_Fake();

            Assert.Throws<IndecentProposalError>(
                delegate
                {
                    worker.Handle(work);
                }
            );
        }

        [Test]
        public void Handle_CanHandle_Returns_True_Should_Not_Throw_Anything()
        {
            IWorker worker = MakeWorker(EnsureCanHandle.ReturnsTrue);
            IWork work = new Work_Fake();

            Assert.DoesNotThrow(
                delegate
                {
                    worker.Handle(work);
                }
            );
        }
    }

    /*
    [TestFixture(typeof(FirstJointer))]
    [TestFixture(typeof(SecondJointer))]
    class IJointer_Contract<T> where T : IJointer, new() // new() is needed to be able to create new instances of T
    {
        IJointer MakeJointer() 
        {
            return new T();
        }

     */
}
