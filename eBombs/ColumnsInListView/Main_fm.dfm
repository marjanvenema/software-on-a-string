object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 308
  ClientWidth = 381
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    381
    308)
  PixelsPerInch = 96
  TextHeight = 13
  object ListView: TListView
    Left = 8
    Top = 8
    Width = 366
    Height = 292
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <>
    TabOrder = 0
    ViewStyle = vsReport
  end
  object RB_vsIcon: TRadioButton
    Left = 281
    Top = 16
    Width = 80
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'vsIcon'
    TabOrder = 1
    OnClick = RB_vsIconClick
  end
  object RB_vsSmallIcon: TRadioButton
    Left = 281
    Top = 56
    Width = 80
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'vsSmallIcon'
    TabOrder = 2
    OnClick = RB_vsIconClick
  end
  object RB_vsList: TRadioButton
    Left = 281
    Top = 96
    Width = 80
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'vsList'
    TabOrder = 3
    OnClick = RB_vsIconClick
  end
  object RB_vsReport: TRadioButton
    Left = 281
    Top = 136
    Width = 80
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'vsReport'
    TabOrder = 4
    OnClick = RB_vsIconClick
  end
  object SQLQuery: TSQLQuery
    Params = <>
    Left = 24
    Top = 24
  end
end
