unit Main_fm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, FMTBcd, DB, SqlExpr, StdCtrls;

type
  TMainForm = class(TForm)
    ListView: TListView;
    SQLQuery: TSQLQuery;
    RB_vsIcon: TRadioButton;
    RB_vsSmallIcon: TRadioButton;
    RB_vsList: TRadioButton;
    RB_vsReport: TRadioButton;
    procedure FormShow(Sender: TObject);
    procedure RB_vsIconClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  RB_vsIcon.Tag := Ord(vsIcon);
  RB_vsSmallIcon.Tag := Ord(vsSmallIcon);
  RB_vsList.Tag := Ord(vsList);
  RB_vsReport.Tag := Ord(vsReport);

  RB_vsIcon.Visible := False;
  RB_vsSmallIcon.Visible := False;
  RB_vsList.Visible := False;
  RB_vsReport.Visible := False;
end;

procedure TMainForm.RB_vsIconClick(Sender: TObject);
begin
  ListView.ViewStyle := TViewStyle((Sender as TRadioButton).Tag);
end;

procedure TMainForm.FormShow(Sender: TObject);
const
  RECORD_VALUES: array[1..5] of record
    FirstName: string;
    LastName: string;
    Country: string;
  end = (
      (FirstName: 'Marlene'; LastName: 'Dietrich'; Country: 'Germany')
    , (FirstName: 'Brigitte'; LastName: 'Bardot'; Country: 'France')
    , (FirstName: 'Marilyn'; LastName: 'Monroe'; Country: 'USA')
    , (FirstName: 'Catherine'; LastName: 'Deneuve'; Country: 'France')
    , (FirstName: 'Katharine'; LastName: 'Hepburn'; Country: 'USA')
  );
var
  idxRecord: Integer;
  Column: TListColumn;
  Item: TListItem;
begin
  ListView.Clear;
  ListView.ViewStyle := vsReport;

//  SQLQuery.SQL.Text := 'SELECT FirstName, LastName, Country FROM Famous_Actresses';
//  SQLQuery.Open;
//  while not SQLQuery.Eof do
  for idxRecord := Low(RECORD_VALUES) to High(RECORD_VALUES) do
  begin
//    ListView.AddItem('First name: ' + SQLQuery['FirstName'], nil);
//    ListView.AddItem('Last name: ' + SQLQuery['LastName'], nil);
//    ListView.AddItem('Country: ' + SQLQuery['Country'], nil);

//    ListView.AddItem('First name: ' + RECORD_VALUES[idxRecord].FirstName, nil);
//    ListView.AddItem('Last name: ' + RECORD_VALUES[idxRecord].LastName, nil);
//    ListView.AddItem('Country: ' + RECORD_VALUES[idxRecord].Country, nil);
  end;

  // Set up columns
  Column := ListView.Columns.Add;
  Column.Caption := 'First Name';
  Column.Alignment := taLeftJustify;
  Column.Width := -1;

  Column := ListView.Columns.Add;
  Column.Caption := 'Last Name';
  Column.Alignment := taLeftJustify;
  Column.Width := -1;

  Column := ListView.Columns.Add;
  Column.Caption := 'Country';
  Column.Alignment := taLeftJustify;
  Column.Width := 140;

  // Add values
  for idxRecord := Low(RECORD_VALUES) to High(RECORD_VALUES) do
  begin
    Item := ListView.Items.Add;
    Item.Caption := RECORD_VALUES[idxRecord].FirstName;
    Item.SubItems.Add(RECORD_VALUES[idxRecord].LastName);
    Item.SubItems.Add(RECORD_VALUES[idxRecord].Country);
  end;

end;

end.
